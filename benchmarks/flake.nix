{
  description = "Dynres Workbench";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    NUR.url = "github:nix-community/NUR";
    nxc.url = "gitlab:nixos-compose/nixos-compose/23.11?host=gitlab.inria.fr";
    #kapack.url = "path:/home/auguste/dev/nur-kapack/dynres";
    kapack.url = "github:oar-team/nur-kapack?ref=dynres";
    kapack.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, nxc, NUR, kapack }:
  let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
    # need for dmr_examples with use lkm (unfree lib from Intel)
    config.allowUnfree = true; 
  in {
    packages.${system} = nxc.lib.compose {
      inherit nixpkgs system NUR;
      repoOverrides = { inherit kapack; };
      composition = ./composition.nix;
      setup = ./setup.toml;
    };
    
    defaultPackage.${system} =
        self.packages.${system}."composition::docker";
    devShell.${system} = nxc.devShells.${system}.nxcShell;

    benchmarks-info =
      let
        kapack-packages = with kapack.packages.${system}; [
          pmix-dynres
          pypmix-dynres
          prrte-dynres
          prrte-expansion-test
          openmpi-dynres
          miniapps-dynres
          dyn_rm-dynres 
          dyn_psets
          dyn_rm-examples-dynres
          oac
          ucc_1_3
          ucx_1_17
          dmr
          dmr-examples
          timestamps
          data_redist
          p4est-sc-dynres
          p4est-dynres
          p4est-dyn
          p4est-dyn-examples
          dynpetsc
          sowing
          dynpetsc-examples
          xbraid
          dyn-xbraid
          dyn-xbraid-examples
          benchmarks-dynres
          p4est-dyn-examples
          dynpetsc-examples
          dyn-xbraid-examples
        ];
        
        getPackageInfo = pkg: {
          name = pkg.pname or pkg.name;
          version = pkg.version or "unknown";          
          rev = if builtins.hasAttr "src" pkg && pkg.src != null && builtins.hasAttr "rev" pkg.src 
                then pkg.src.rev 
                else "unknown"; 
        };
        
        packageInfo = builtins.map getPackageInfo kapack-packages;
        
        json = builtins.toJSON packageInfo;
      in
        pkgs.writeText "benchmarks-info.json" json;

    
  };
}

