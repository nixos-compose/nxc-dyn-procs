{ pkgs, lib, modulesPath, ... }: {
  roles = let
    # Unsecured ssh keys, use only for test purpose
    inherit (import "${toString modulesPath}/tests/ssh-keys.nix" pkgs)
      snakeOilPrivateKey snakeOilPublicKey;
  in {
    # Only one role "deployed" n times 
    n = { ... }: {
      
      #####################################
      # Declaration of mpiuser with sshkeys
      #
      # support of predefined keys should be provided as nxc module
      # nxc.users = { names = ["user1"]; prefixHome = "/users"; snakeOilKeys = true; };
      users.users.mpiuser = {
        isNormalUser = true;
        openssh.authorizedKeys.keys = [ snakeOilPublicKey ];
      };

      programs.ssh.extraConfig = ''
        UserKnownHostsFile=/dev/null
        StrictHostKeyChecking=no
      '';

      systemd.services.nxc-script = {
        after = [ "network.target" "network-online.target" ];
        wants = [ "network-online.target" ];
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          Type = "oneshot";
          Restart = "on-failure";
          RestartSec = 1;
        };
        script = ''
          mkdir /home/mpiuser/.ssh
          echo ${snakeOilPublicKey} >/home/mpiuser/.ssh/id_rsa.pub
          chmod 600 /home/mpiuser/.ssh/id_rsa.pub
          chown -R mpiuser /home/mpiuser/.ssh
        '';
      };

      
      
      ################################
      # Installed Packages
      #
      # Dynres packages are defined in dynres branch of nur-kapack:
      # https://github.com/oar-team/nur-kapack/tree/dynres/pkgs
      environment.systemPackages = let p3 = pkgs.python3.withPackages (ps: [ ps.pyyaml ]); in [
        pkgs.nur.repos.kapack.prrte-dynres
        pkgs.nur.repos.kapack.openmpi-dynres
        pkgs.nur.repos.kapack.miniapps-dynres
        pkgs.nur.repos.kapack.prrte-expansion-test
        pkgs.nur.repos.kapack.dyn_rm-examples-dynres
        pkgs.nur.repos.kapack.dmr-examples
        pkgs.nur.repos.kapack.p4est-dyn-examples
        pkgs.nur.repos.kapack.dynpetsc-examples
        pkgs.nur.repos.kapack.dyn-xbraid-examples
        pkgs.nur.repos.kapack.benchmarks-dynres
        pkgs.gcc
        # python3 w/ pyyaml, execo
        (pkgs.python3.withPackages (ps: [ ps.pyyaml  pkgs.nur.repos.kapack.execo]))        
      ];

      ######################################
      # Addtionnal env. variable declaration
      environment.variables = {
        # Env. var. to access to dyn_rm examples
        DYN_RM_EXAMPLES = "${pkgs.nur.repos.kapack.dyn_rm-examples-dynres}";
        DYN_RM = "${pkgs.nur.repos.kapack.dyn_rm-dynres}";
        DYN_RM_PMIX_PATH = "${pkgs.nur.repos.kapack.pmix-dynres}";
        DMR_EXAMPLES = "${pkgs.nur.repos.kapack.dmr-examples}";
        DYN_P4EST_EXAMPLES = "${pkgs.nur.repos.kapack.p4est-dyn-examples}";
        DYN_PETSC_EXAMPLES = "${pkgs.nur.repos.kapack.dynpetsc-examples}";
        DYN_XBRAID_EXAMPLES = "${pkgs.nur.repos.kapack.dyn-xbraid-examples}";
        DYN_BENCHMARKS = "${pkgs.nur.repos.kapack.benchmarks-dynres}";
        DYN_BENCHMARKS_SRC = "${pkgs.nur.repos.kapack.benchmarks-dynres.src}";
        # TODO: Fix dyn_rm/mca/callback/modules/pmix/pmix_callback.py to remove the following 3 envars
        LIBRARY_PATH="/tmp"; 
        LD_LIBRARY_PATH = "/tmp";
        C_INCLUDE_PATH = "/tmp";
        #PYTHONPATH="${pkgs.nur.repos.kapack.pmix-dynres}/lib/python3.10/site-packages"; # Should be removed
      };
    };
  };
  ##############################
  # Default number for each role 
  rolesDistribution = { n = 8; }; # n1 ... n8

  ###########################################
  # Test executed when "driver -t" is invoked
  # Todo
  testScript = ''
    #n1.succeed("su mpiuser -c ' ' ")
    #n1.succeed("su mpiuser -c ' ' ")  
  '';
}
