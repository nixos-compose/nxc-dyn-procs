
Experiment Dyn-Procs with help of NixOS-Compose
===============================================================

# Why
With [NixOS-Compose (NXC)](https://github.com/oar-team/nixos-compose) only one *[composition](nxc/composition.nix)* is sufficent to
describe the all setup with guarantee or reproducibility and support for multiple targets (VM, Docker, Grid'5000). 

Dynres packages (pmix, prrte, openmpi, miniapps and dyn_rm) are defined in dynres branch of nur-kapack project: [nur-kapack](https://github.com/oar-team/nur-kapack/tree/dynres/pkgs)

# Installation

## 1. Install Nixos-Compose whitout Nix
 - Installation
 ```bash
 pip install nixos-compose
 ```
 - You might need to modify your `$PATH`:
 ```bash
 export PATH=$PATH:~/.local/bin
  ```
 - To upgrade
 ```bash
 pip install --upgrade nixos-compose
 ```
 
 - The following command will install a standalone and static Nix version in `~/.local/bin`
 ```bash
 nxc helper install-nix
 ```
 
## 2. Clone this repository

 ```bash
 git clone git@gitlab.inria.fr:nixos-compose/nxc-dyn-procs.git
 ```

# Use

## Common



## 1. Docker

### Allow unfree packages for  mkl

```bash
export NIXPKGS_ALLOW_UNFREE=1
```

### Build

```bash
nxc build -f docker
```

### Start / Stop
```bash
nxc start
nxc stop
```

### Connect

```bash
# connect as mpiuser on node n01
nxc connect -l mpiuser n1
# to connect and spawn new tmux with pane for each node
nxc connect
# to connect as mpiuser using the "test" private key
nxc connect -l mpiuser -i ./ssh/id_rsa
```

### Launch some commands (after nxc connect)
```bash
   cd
   $DYN_RM_EXAMPLES/run_test_dynrm \
   --topology_file=$DYN_RM_EXAMPLES/topology_files/8_node_system.yaml \
   --submission_file=$DYN_RM_EXAMPLES/submissions/sleep_expand_dynrm_nb.batch \
   --verbosity=9 \
   --output_dir=/tmp/expansion_test \
   --step_size=single
    
   $DMR_EXAMPLES/run_test_dynrm \
   --topology_file=$DMR_EXAMPLES/topology_files/8_node_system.yaml \
   --submission_file=$DMR_EXAMPLES/submissions//jacobi_expand.batch \
   --verbosity=11 \
   --output_dir=/tmp/output_dmr_example \
   --step_size=single \
   --system=PrrteSingleInstanceSystem
   
   $DYN_P4EST_EXAMPLES/run_test_dynrm \
   --topology_file=$DYN_P4EST_EXAMPLES/topology_files/8_node_system_sparse.yaml \
   --submission_file=$DYN_P4EST_EXAMPLES/submissions/p4est_expand.batch \
   --verbosity=11 \
   --output_dir=/tmp/output_dyn_p4est_example\
   --step_size=linear \
   --system=PrrteSingleInstanceSystem
   
   $DYN_PETSC_EXAMPLES/run_test_dynrm \
   --topology_file=$DYN_PETSC_EXAMPLES/topology_files/8_node_system_sparse.yaml \
   --submission_file=$DYN_PETSC_EXAMPLES/submissions/petsc_ts_expand.batch \
   --verbosity=11 \
   --output_dir=/tmp/output_dyn_petsc_example \
   --step_size=linear \
   --system=PrrteSingleInstanceSystem 
 
   $DYN_XBRAID_EXAMPLES/run_test_dynrm \
   --topology_file=$DYN_XBRAID_EXAMPLES/topology_files/8_node_system_sparse.yaml \
   --submission_file=$DYN_XBRAID_EXAMPLES/submissions/dyn_xbraid_expand.batch \
   --verbosity=11 \
    --output_dir=/tmp/output_dyn_xbraid_example \
   --step_size=linear \
   --system=PrrteSingleInstanceSystem 
```

#### Execute testscript (from outside)
```bash
nxc drive -t
```




## 2. Grid'5000

### Grid'5000 account
- Go https://www.grid5000.fr/w/Grid5000:Get_an_account
- You can as ask for regular account if you have an active formal collaboration with academic researchers from France (e.g. via a European project) or a special agreement, otherwise ask for open access one

### Installation
- Repeat previous installation without Nix
- Clone this repo

### Build image to deploy
```bash
# build on dedicated node not on frontend 
# reserve one node
oarsub -I
# go to nxc-dyn-procs
cd nxc-dyn-procs
# build image (g5k-nfs-store image is the fastest to deploy)
nxc build -f g5k-nfs-store
```

### Deploy image on nodes

```bash
# reserve some 4 nodes for 2 hours and retrieve $OAR_JOB_ID in one step
export $(oarsub -l nodes=4,walltime=2:0 "$(nxc helper g5k_script) 2h" | grep OAR_JOB_ID)
# deploy (use last built image)
nxc start -m ./OAR.$OAR_JOB_ID.stdout -W
# connect and spawn new tmux with pane for each node
nxc connect
```
**Note:** *nxc connect* can be use to connect to only one node *nxc connect <node>*. Also **nxc connect** is really useful only if a minimal set of **[tmux](https://github.com/tmux/tmux/wiki/Getting-Started)**'s key bindings are mastered (like Ctrl-b + Up, Down, Right, Left to change pane, see tmux manual for other key bindings.


