{ pkgs, modulesPath, nur, flavour }:
let
  inherit (import "${toString modulesPath}/tests/ssh-keys.nix" pkgs)
    snakeOilPrivateKey snakeOilPublicKey;

  scripts = import ./scripts/scripts.nix { inherit pkgs; };

in {
  imports = [ nur.repos.kapack.modules.oar ];
  environment.systemPackages = with pkgs; [
    perl # TODO add to oar package
    python3
    vim
    nur.repos.kapack.oar
    jq
    redis
    scripts.start_dynrm_poc_1
    scripts.next_step_poc_1
  ];
  
  nxc.users = { names = ["user1" "user2"]; prefixHome = "/users"; };
 
  services.openssh.extraConfig = ''
    AuthorizedKeysCommand /usr/bin/sss_ssh_authorizedkeys
    AuthorizedKeysCommandUser nobody
  '';

  # security.pam.loginLimits = [
  #   { domain = "*"; item = "memlock"; type = "-"; value = "unlimited"; }
  # ];
  
  environment.etc."privkey.snakeoil" = {
    mode = "0600";
    source = snakeOilPrivateKey;
  };
  environment.etc."pubkey.snakeoil" = {
    mode = "0600";
    text = snakeOilPublicKey;
  };

  environment.etc."oar-dbpassword".text = ''
    # DataBase user name
    DB_BASE_LOGIN="oar"

    # DataBase user password
    DB_BASE_PASSWD="oar"

    # DataBase read only user name
    DB_BASE_LOGIN_RO="oar_ro"

    # DataBase read only user password
    DB_BASE_PASSWD_RO="oar_ro"
  '';


  services.oar = {
    extraConfig = {
      LOG_LEVEL = "3";
      HIERARCHY_LABELS = "resource_id,network_address,cpuset";
      JOB_RESOURCE_MANAGER_FILE =
        "/etc/oar/job_resource_manager_evolving_nixos.pl";
      #EXTRA_METASCHED = "dyn_rm";
      OAREXEC_DEBUG_MODE = "1";
    };

    # oar db passwords
    database = {
      host = "server";
      passwordFile = "/etc/oar-dbpassword";
      initPath = [ pkgs.util-linux pkgs.gawk pkgs.jq scripts.add_resources ];
      postInitCommands = ''
        num_cores=$(( $(lscpu | awk '/^Socket\(s\)/{ print $2 }') * $(lscpu | awk '/^Core\(s\) per socket/{ print $4 }') ))
        echo $num_cores > /etc/num_cores

        if [[ -f /etc/nxc/deployment-hosts ]]; then
          num_nodes=$(grep node /etc/nxc/deployment-hosts | wc -l)
        else
          num_nodes=$(jq -r '[.nodes[] | select(contains("node"))]| length' /etc/nxc/deployment.json)
        fi
        echo $num_nodes > /etc/num_nodes

        add_resources $num_nodes $num_cores
      '';
    };
    server.host = "server";
    privateKeyFile = "/etc/privkey.snakeoil";
    publicKeyFile = "/etc/pubkey.snakeoil";
  };

}
