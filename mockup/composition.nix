# Note:
# - redis service is UNSECURED (no password protected, open to attack)
# 

{ pkgs, modulesPath, nur, helpers, flavour, ... }: {
  #dockerPorts.frontend = [ "8443:443" "8000:80" ];
  roles = let
    commonConfig =
      import ./common_config.nix { inherit pkgs modulesPath nur flavour; };
  in {
    frontend = { ... }: {
      imports = [ commonConfig ];
      services.oar.client.enable = true;
      services.oar.node.enable = true;
      #services.oar.web.enable = true;
      #services.oar.web.drawgantt.enable = true;
      # for shared dir (nfs on vm/g5k) 
      nxc.sharedDirs."/users".server = "server";
    };
    server = { ... }: {
      imports = [ commonConfig ];
      services.oar.server.enable = true;
      #services.oar.package = pkgs.oar-with-plugins;
      services.oar.dbserver.enable = true;
      services.redis.servers.oar = {
        enable = true;
        port = 6379;
        #requirePassFile =  pkgs.writeText "redis-oar-pass" "oar";
        bind = "0.0.0.0"; # listen on all interface for remote connection
        settings = { protected-mode = "no"; }; # remote connection w/o passwd
      };
      # for shared dir (nfs on vm/g5k) 
      nxc.sharedDirs."/users".export = true;

    };
    node = { ... }: {
      imports = [ commonConfig ];
      services.oar.node = { enable = true; };
      # for shared dir (nfs on vm/g5k) 
      nxc.sharedDirs."/users".server = "server";
      
    };
  };

  ##############################
  # Default number for each role
  rolesDistribution = { node = 4; }; # node1 ... node4

  testScript = ''
    # test UNSECURED server' redis 
    #frontend.succeed("${pkgs.redis}/bin/redis-cli -h server ping | grep PONG")
    frontend.succeed("true")
    # Prepare a simple script which execute cg.C.mpi 
    frontend.succeed('echo "mpirun --hostfile \$OAR_NODEFILE -mca pls_rsh_agent oarsh -mca btl tcp,self cg.C.mpi" > /users/user1/test.sh')
    # Set rigth and owner of script
    frontend.succeed("chmod 755 /users/user1/test.sh && chown user1 /users/user1/test.sh")
    # Submit job with script under user1
    frontend.succeed('su - user1 -c "cd && oarsub -l nodes=2 ./test.sh"')
    # Wait output job file 
    frontend.wait_for_file('/users/user1/OAR.1.stdout')
    # Check job's final state
    frontend.succeed("oarstat -j 1 -s | grep Terminated")
  '';
}
