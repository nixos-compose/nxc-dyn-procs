{ pkgs }:
with pkgs.writers; {
  start_dynrm_poc_1 = pkgs.writeScriptBin "start_dynrm_poc_1.sh" (builtins.readFile ./start_dynrm_poc_1.sh);
  next_step_poc_1 = pkgs.writeScriptBin "next_step_poc_1.sh" (builtins.readFile ./next_step_poc_1.sh);  
     
  #start_dynrm = pkgs.writeScriptBin "start_dynrm.sh" (builtins.readFile ./start_dynrm.sh);
  #next_step = pkgs.writeScriptBin "next_step.sh" (builtins.readFile ./next_step.sh);  

  # add_resources is used to speed the filling of the OAR's database.
  add_resources = writePython3Bin "add_resources" {
    libraries = [ pkgs.nur.repos.kapack.oar ];
  } ''
    from oar.lib.tools import get_date
    from oar.lib.resource_handling import resources_creation
    from oar.lib.globals import init_and_get_session
    import sys
    import time
    r = True
    n_try = 10000

    session = None
    # wait database
    while n_try > 0 and r:
        n_try = n_try - 1
        try:
            session = init_and_get_session()
            print(get_date(session))  # date took from db (test connection)
            r = False
        except Exception:
            print("DB is not ready")
            time.sleep(0.25)

    if session:
        resources_creation(session, "node", int(sys.argv[1]), int(sys.argv[2]))
        print("resource created")
    else:
        print("resource creation failed")
  '';

}
