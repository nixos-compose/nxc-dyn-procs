#!/usr/bin/env bash

echo "Use POC variants....(WIP), like start_dynrm_poc_1.sh"

exit 1 


help() {
	echo
	echo "options:"
	echo "h     Print this Help."
	echo "a "
}


APP=""

PRRTE_START=0
PRRTE_PARAMS=""
OAR_REQUEST_PARAMS=""

WALLTIME=3600

while getopts ":a:r:w:hp" option; do
	case $option in
	h) # display Help
		help
		exit
		;;
	a)
		echo "submit job for application: -a: ${OPTARG}"
        APP=$OPTARG
		;;
	r)
		echo "oar request parameters minus: -t leaflet and command : ${OPTARG}"
        OAR_REQUEST_PARAMS=$OPTARG
		;;
    
    w)
		echo "Walltime in sec: ${OPTARG}"
        WALLTIME=$OPTARG
		;;
    p)
		echo "Submit start_prrte: ${OPTARG}"
        PRRTE_START=1
		;;
	esac
done

echo OAR_JOB_ID for start_dynrm: $OAR_JOB_ID
echo "launch dynrm (fake do nothing for now)"


if [[ $APP ]]; then
    echo "Submit the first step of $APP (TODO it's just a placeholder)"
    echo "oarsub -t leaflet=$OAR_JOB_ID $APP (TODO it's just a placeholder)"  
fi

if [ $PRRTE_START -eq 1 ]; then
    echo "Submit job to launch PRRTE"
    echo "oarsub -t leaflet=$OAR_JOB_ID $OAR_REQUEST_PARAMS start_prrte -e=$OAR_JOB_ID $PRRTE_PARAMS" 
fi

echo "Wait $WALLTIME"
sleep $WALLTIME
