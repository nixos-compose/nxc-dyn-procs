#!/usr/bin/env bash
# 
# OAR-DYNRM: POC-1 base on external OAR interactions NO PLUGINS
#
# - ENVELOPE_JID = oarsub -t envelope -l nodes=1 "start_dynrm_poc_1.sh apps params"
#     - start dynrm
#     - dynrm:
#       - start prte master
#       - define initial resource requirement, tyically number of nodes
#       - JID_1 = oarsub -t leaflet=$ENVELOPE_JID -l nodes=xxx next_step.sh (prte args ?)
#       - next_step.sh:
#          - prepare cpuset and other stuff if needed (???)
#          - start prrte agent only if no running prte_ENVELOPE_JID
#
#     - dynrm:
#       - wait next_step.sh start / cpuset 
#       - start application w/ prte commands
#
#   - New step (Expand oar Shrink) approach 1 (minimalist)
#     - dynrm
#       - no specific discovery phase:
#         - done out-of-band or
#         - pulling w/ oarstat w/ or w/o
#           oarstat --gantt --json "YYYY-MM-DD hh:mm:ss, YYYY-MM-DD hh:mm:ss"
#           decide next step 
#       - submit next step
#         - JDx = oarsub -t leaflet=$ENVELOPE_JID -t clean=keep-children -t supersed=JID_previous_step -l nodes=4 -p  "host IN ('n1', 'n2', 'n3', 'n4')" next_step.sh
#         - next_step.sh:
#            - prepare cpuset and other stuff if needed
#            - start prrte agent only if no running prte_ENVELOPE_JID
#
#     - dynrm:
#       - wait next_step.sh start / cpuset 
#       - dynrm start application w/ prte commands 
#       - send signal to previous next_step.sh job to quit
#
#     - New step -> Repeat

echo "OAR-DYNRM: POC-1 using only external OAR interactions (cli or restful api)"

if [ -z "$OAR_JOB_ID" ]; then
    echo "Please submit a oar job: oarsub -t envelope start_dynrm_poc_1.sh"
    exit 1
else
    ENVELOPE_JOB_ID=$OAR_JOB_ID
    echo "ENVELOPE_JOB_ID=$ENVELOPE_JOB_ID"
fi

echo "[TODO] start dynrm"
echo "[TODO] dynrm: start prte master, retrieve/set params" 

echo "dynrm: submit first step: next_step.sh"
echo "oarsub -t leaflet=$ENVELOPE_JOB_ID -l nodes=1 \"next_step_poc_1.sh params1 params2\""

export $(oarsub -t leaflet=$ENVELOPE_JOB_ID -l nodes=1 "next_step_poc_1.sh params1 params2" | grep OAR_JOB_ID)

echo "First (next_)step with job id: $OAR_JOB_ID is submitted"

echo "Waiting for OAR.$OAR_JOB_ID.stdout (clumsy start detection of next_step)"
# just a simple synchro for POC
file_to_wait="OAR.$OAR_JOB_ID.stdout"
while [ ! -f ${file_to_wait} ]
do
  echo -n .  
  sleep 1
done

echo "cpuset stuff is set (and other init on nodes if needed)"
echo "[TODO] dynrm: launch prte agents on new nodes"
    
echo "[TODO] dynrm: start application w/ prte commands"

echo "Application is running (waiting)"
echo "Only wait 5 sec (for demo)"
sleep 5

echo "[TODO] dynrm: Time to expand"
echo "[TODO] dynrm: decides new EXPANDING step based on pulling oarstat (by example)"
echo '   oarstat --gantt --json "YYYY-MM-DD hh:mm:ss, YYYY-MM-DD hh:mm:ss'

echo "dynrm: submits next step (with fixed resources)"
echo "      oarsub -t leaflet=$ENVELOPE_JOB_ID -t supersed=$OAR_JOB_ID -l nodes=3 -p  \"network_address in ('node1', 'node2', 'node3', 'node4')\" \"next_step_poc_1.sh params1 params2\""

PREVIOUS_NEXT_STEP_JOB_ID=$OAR_JOB_ID

export $(oarsub -t leaflet=$ENVELOPE_JOB_ID -t supersed=$OAR_JOB_ID -l nodes=3 -p "network_address in ('node1', 'node2', 'node3', 'node4')" "next_step_poc_1.sh  params1 params2" | grep OAR_JOB_ID)

echo "Then next step with job id: $OAR_JOB_ID is submitted"

echo "Waiting for OAR.$OAR_JOB_ID.stdout (clumsy start detection of next_step)"
# just a simple synchro for POC
file_to_wait="OAR.$OAR_JOB_ID.stdout"
while [ ! -f ${file_to_wait} ]
do
  echo -n .  
  sleep 1
done

echo "cpuset stuff is set (and other init on nodes if needed)"
echo "[TODO] dynrm: launch prte agent on only NEW nodes"
echo "[TODO] dynrm: start expand prte commands"

echo "Application's next step (is running)"
echo "dynrm: send signal to previous next_step job to ask to exit/clean: PREVIOUS_NEXT_STEP_JOB_ID=$PREVIOUS_NEXT_STEP_JOB_ID"
echo " Signal is sent to head node script pf job"
echo "    oardel -s SIGUSR1 $PREVIOUS_NEXT_STEP_JOB_ID"

oardel -s SIGUSR1 $PREVIOUS_NEXT_STEP_JOB_ID

echo "Only wait 5 sec (for demo)"
sleep 5
echo "[TODO] Time to shrink......"

