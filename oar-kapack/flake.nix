{
  description = "OAR - basic setup";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/23.05";
    nxc.url = "gitlab:nixos-compose/nixos-compose/23.05?host=gitlab.inria.fr";
    nxc.inputs.nixpkgs.follows = "nixpkgs";
    kapack.url = "github:oar-team/nur-kapack/dynres";
    kapack.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, nxc, kapack}:
    let
      system = "x86_64-linux";
    in {
      packages.${system} = nxc.lib.compose {
        inherit nixpkgs system ;
        #extraConfigurations = [ kapack.nixosModules.oar ];
        overlays = [
           #(self: super: {
           #  oar = kapack.packages.${system}.oar;
           #  oar-with-plugins = kapack.packages.${system}.oar-with-plugins;
           #})
        ];
        setup = ./setup.toml;
        composition = ./composition.nix;
        };

      devShell.${system} = nxc.devShells.${system}.nxcShell;
     };
}

