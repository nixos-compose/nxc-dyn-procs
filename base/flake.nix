{
  description = "Dynres Workbench";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    NUR.url = "github:nix-community/NUR";
    nxc.url = "gitlab:nixos-compose/nixos-compose/23.11?host=gitlab.inria.fr";
    #kapack.url = "path:/home/auguste/dev/nur-kapack/dynres";
    kapack.url = "github:oar-team/nur-kapack?ref=dynres";
    kapack.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, nxc, NUR, kapack }:
  let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
    # need for dmr_examples with use lkm (unfree lib from Intel)
    config.allowUnfree = true; 
  in {
    packages.${system} = nxc.lib.compose {
      inherit nixpkgs system NUR;
      repoOverrides = { inherit kapack; };
      composition = ./composition.nix;
      setup = ./setup.toml;
    };
    
    defaultPackage.${system} =
        self.packages.${system}."composition::docker";
    devShell.${system} = nxc.devShells.${system}.nxcShell;
  };
}

